function submitRequest() {
    var errhdr = 'Ошибка отправки формы'
    var errmsg = 'Сервис временно недоступен, повторите попытку позже.';
    var name   = jQuery('form[name="consultRequest"] input[name="fio"]').val();
    var phone  = jQuery('form[name="consultRequest"] input[name="subject"]').val();

    if (!name || !phone) {
        console.error('send form failure: blank input');

        jQuery('#requestModalLabel').text(errhdr);
        jQuery('#requestModalBody').text(errmsg);
    }
    else {
        console.log('send form: name ' + name + ', phone: ' + phone);

        var jqxhr = jQuery.post('/feedback.html', jQuery('form[name="consultRequest"]').serialize(), function(response) {
            console.log('send form ok');
            //console.log(response);
        })
        .fail(function() {
            jQuery('#requestModalLabel').text(errhdr);
            jQuery('#requestModalBody').text(errmsg);

            console.log('send form failure: jqxhr');
        })
        .always(function() {
            console.log('send form request is finished');

            jQuery('#requestCallBack').modal();
        });
    }

    void(0);
}

function renderNews() {
    var jqxhr = jQuery.getJSON("/news.html", function(data) {
        console.log("get news json ok");
        var items = [];

        for(let i = 0; i < data.length; i++) {
            let obj = data[i];

            if (obj.link) {
                items.push('<li>' + obj.link + '</li>');
            }
        };

        jQuery('<ul/>', {class: 'text-info', html: items.reverse().join('')}).appendTo(jQuery('#consult51-news-container'));
    })
    .fail(function() {
        console.log( "get news failure: jqxhr" );

        jQuery('<div/>', {class: 'alert alert-danger text-center', text: 'Ошибка загрузки ленты новостей'}).appendTo(jQuery('#consult51-news-container'));
    })
    .always(function() {
        console.log( "get json request is finished" );
    });
}
